FROM alpine:latest

RUN apk --no-cache add dnsmasq sniproxy

COPY root/ /

EXPOSE 53 53/udp 80 443 

ENTRYPOINT ["/startup.sh"]
