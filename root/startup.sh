#! /bin/sh

if [ "${IFACE}" = "" ]; then
  IFACE=${__HOME_INTERFACE}
fi
if [ "${IFACE}" = "" ]; then
  IFACE=eth0
fi
if [ "${PROXYIP}" = "" ]; then
  PROXYIP=$(ip addr show dev ${IFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)
fi

if [ "${PROXYDNS}" != "" ]; then
  cat /etc/resolv.preconf | sed "s/{{PROXYDNS}}/${PROXYDNS}/g" > /etc/resolv..conf
fi

for i in /etc/dnsmasq.d/*.preconf
do
  cat $i | sed "s/{{PROXYIP}}/${PROXYIP}/g" > $(dirname $i)/$(basename $i .preconf).conf
done

dnsmasq
sniproxy -c /etc/sniproxy/sniproxy.conf

# Run until stopped
trap "exit" TERM
sleep 2147483647d &
wait "$!"
